-- 用户表
create table user
(
  id integer not null primary key autoincrement,
  username varchar not null,
  password varchar not null,
  nickname varchar comment '昵称',
  signature varchar comment '签名',
  create_at timestamp default current_date
)
;

-- 好友
create table freind
(
  id integer not null primary key autoincrement,
  user_id integer not null,
  freind_user_id integer not null,
  freind_group_id integer
);

-- 好友分组
create table freind_group
(
  id integer not null primary key autoincrement,
  user_id integer not null,
  name varchar not null,
  order integer default 0
);
